# Docker SpringBoot CRUD POC

## About

    - Docker SpringBoot Proof of Concept
    
## Usage

    - src: source app code
    - target: app files
    - mvnw*: maven files
    - create-database.sql: app database sql script
    - docker-compose.yml: environment container configuration file
    
## Dependeces
    - Docker 19+
    - docker-compose 1.25+
    - curl 7.+
    - git 2.20.+

## Download

    - git clone https://gitlab.com/cedon/stdrpoc.git 
    - cd stdrpoc/

## Tutorial

    - command to execute the app(on app folder): 
    `$ sudo docker-compose up

## User manual

    - CRUD:
    - access `host_ip:8080/service/all` to list
    - access curl `host_ip:8080/service/add` to add
    `example $ curl host_ip:8080/cliente/add -d name=Carlos -d email=carlos@someemailprovider.com \n
    -d fone=91981543641 -d cpf=123654789

    - Debug MySQL(PHPMyAdmin):
    - access host_ip/

### Services

    - cliente(name,email,fone,cpf)
    - restaurante(nome,email,fone,cpf,id_geolocalizacao)
    - prato(nome,descricao,valor)
    - bebida(nome,descricao,valor)
    - tipocomida(nacionalidade)
    - pedido(id_prato,id_bebida,id_cliente,id_restaurante)
    