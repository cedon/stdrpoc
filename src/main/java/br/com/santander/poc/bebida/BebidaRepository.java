package br.com.santander.poc.bebida;

import org.springframework.data.repository.CrudRepository;

import br.com.santander.poc.bebida.Bebida;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface BebidaRepository extends CrudRepository<Bebida, Integer> {

}
