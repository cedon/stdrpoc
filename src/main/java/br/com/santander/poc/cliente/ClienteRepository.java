package br.com.santander.poc.cliente;

import org.springframework.data.repository.CrudRepository;

import br.com.santander.poc.cliente.Cliente;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

}
