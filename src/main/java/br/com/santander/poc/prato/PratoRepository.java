package br.com.santander.poc.prato;

import org.springframework.data.repository.CrudRepository;

import br.com.santander.poc.prato.Prato;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface PratoRepository extends CrudRepository<Prato, Integer> {

}
