package br.com.santander.poc.pedido;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class Pedido {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Integer id;
  private Integer id_prato;
  private Integer id_bebida;
  private Integer id_cliente;
  private Integer id_restaurante;

  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public Integer getId_prato() {
    return id_prato;
  }
  public void setId_Prato(Integer id_prato) {
    this.id_prato = id_prato;
  }
  public Integer getId_bebida() {
    return id_bebida;
  }
  public void setId_Bebida(Integer id_bebida) {
    this.id_bebida = id_bebida;
  }
  public Integer getId_cliente() {
    return id_cliente;
  }
  public void setId_Cliente(Integer id_cliente) {
    this.id_cliente = id_cliente;
  }
  public Integer getId_Restaurante() {
    return id_restaurante;
  }
  public void setId_Restaurante(Integer id_restaurante) {
    this.id_restaurante = id_restaurante;
  }
}
