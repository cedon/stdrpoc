package br.com.santander.poc.cartao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class Cartao {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Integer id;
  private String nome;
  private String numero;
  private String validade;
  private String cod_verificacao;

  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getNome() {
    return nome;
  }
  public void setNome(String nome) {
    this.nome = nome;
  }
  public String getNumero() {
    return numero;
  }
  public void setNumero(String numero) {
    this.numero = numero;
  }
  public String getValidade() {
    return validade;
  }
  public void setValidade(String validade) {
    this.validade = validade;
  }
  public String getCod_verificacao() {
    return cod_verificacao;
  }
  public void setCod_verificacao(String cod_verificacao) {
    this.cod_verificacao = cod_verificacao;
  }
}
