package br.com.santander.poc.cartao;

import org.springframework.data.repository.CrudRepository;

import br.com.santander.poc.cartao.Cartao;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

}
