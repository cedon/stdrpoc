package br.com.santander.poc.tipocomida;

import org.springframework.data.repository.CrudRepository;

import br.com.santander.poc.tipocomida.TipoComida;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface TipoComidaRepository extends CrudRepository<TipoComida, Integer> {

}
