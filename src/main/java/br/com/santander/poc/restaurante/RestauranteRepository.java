package br.com.santander.poc.restaurante;

import org.springframework.data.repository.CrudRepository;

import br.com.santander.poc.restaurante.Restaurante;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface RestauranteRepository extends CrudRepository<Restaurante, Integer> {

}
