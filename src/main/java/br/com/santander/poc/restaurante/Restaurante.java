package br.com.santander.poc.restaurante;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class Restaurante {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Integer id;
  private String nome;
  private String email;
  private String fone;
  private String cnpj;
  private Integer id_geolocalizacao;

  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getNome() {
    return nome;
  }
  public void setNome(String nome) {
    this.nome = nome;
  }
  public String getEmail() {
    return email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getFone() {
    return fone;
  }
  public void setFone(String fone) {
    this.fone = fone;
  }
  public String getCnpj() {
    return cnpj;
  }
  public void setCnpj(String cnpj) {
    this.cnpj = cnpj;
  }
  public Integer getId_geolocalizacao() {
    return id_geolocalizacao;
  }
  public void setId_geolocalizacao(Integer id_geolocalizacao) {
    this.id_geolocalizacao = id_geolocalizacao;
  }
}
