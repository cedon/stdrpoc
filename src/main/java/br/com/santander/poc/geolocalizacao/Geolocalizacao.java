package br.com.santander.poc.geolocalizacao;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class Geolocalizacao {
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Integer id;
  private String coordenadas;
  private String endereco;

  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }
  public String getCoordenadas() {
    return coordenadas;
  }
  public void setCoordenadas(String coordenadas) {
    this.coordenadas = coordenadas;
  }
  public String getEndereco() {
    return endereco;
  }
  public void setEndereco(String endereco) {
    this.endereco = endereco;
  }
}
