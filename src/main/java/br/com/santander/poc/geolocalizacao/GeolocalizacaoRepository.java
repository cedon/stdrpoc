package br.com.santander.poc.geolocalizacao;

import org.springframework.data.repository.CrudRepository;

import br.com.santander.poc.geolocalizacao.Geolocalizacao;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface GeolocalizacaoRepository extends CrudRepository<Geolocalizacao, Integer> {

}
